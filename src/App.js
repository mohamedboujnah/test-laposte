import './App.scss';
import HomePage from './components/pages/homePage/homePage';

function App() {
  return (
    <>
      <HomePage />
    </>
  );
}

export default App;
