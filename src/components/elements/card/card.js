import React from 'react';

import './card.scss';

function Card({ item }) {

  return (
    <>
      <div id="container">
        <div>
          <p>{item.name}</p>
          <p>{item.full_name}</p>
        </div>
        <div className="info">
          <div>
            <span>{item.watchers}</span>
            <small>Watchers</small>

          </div>
          <div>
            <span>{item.language ? item.language : '...'}</span>
            <small>Language</small>

          </div>
        </div>
      </div>
    </>
  );
}

export default Card;
