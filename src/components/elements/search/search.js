import React from 'react';
import './search.scss';

function Search({ value, onChange, onSubmit, error, children }) {
  return (
    <div  id="search">

      <form className="searchForm" onSubmit={onSubmit}>
        <input
          type="text"
          value={value}
          onChange={onChange}
          placeholder="Chercher un utilisateur..." 
          name="search"
      />
        <button type="submit" onSubmit={onSubmit}><i className="fa fa-search"></i></button>
      </form>
      <span className="error"> {error} </span>
    </div>
  );
}

export default Search;