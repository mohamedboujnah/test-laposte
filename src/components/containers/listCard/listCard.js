import React from 'react';
import { useSelector } from 'react-redux'

import Card from '../../elements/card/card';
import './listCard.scss';

function ListCard() {

  const userData = useSelector(state => state.getUser );

  return (
    <div id="listCard">
      {
        userData && !userData.error ?
          userData.user && userData.user.data ?
            <div>
              {userData.user.data[0] ? <img src={userData.user.data[0].owner.avatar_url} className="avatar" /> : <></>}
              {
                <div className="cardItems">
                  {(userData.user.data || []).map(item => 
                    <Card key={item.id} item={item} /> 
                  )}
                </div>
              } 
            </div>
          : <strong className="center">Chargement1111111111111...</strong>
        : <div></div>      
      }
      {    
        userData.error ? 
          <div className="center error"> {userData.error.message} </div>
        : <div></div>
      }
    </div>
  );
}

export default ListCard;
