import React, {useState} from 'react';
import { useDispatch } from "react-redux";

import { userActions } from '../../../store/actions/user.actions'
import Search from '../../elements/search/search';
import ListCard from '../../containers/listCard/listCard';

const HomePage = () => {
  
  let [search, setSearch] = useState('');
  let [error, setError] = useState('');

  const dispatch = useDispatch();

  // handle Change function
  const handleChange = (event) => {
    setSearch(event.target.value);
  }

  // handle submit function
  const handleSumit = (event) => {
    event.preventDefault();
    setError("")
    if(search)
      dispatch(userActions.getUserRepos(search));
    else
      setError("Le champ ne doit pas étre vide")
  }

  return (
    <>
      <Search value={search} onChange={handleChange} onSubmit={handleSumit} error={error} />
      <ListCard /> 
    </>
  );
};

export default HomePage;