import { userConstants } from '../constants/user.constants';

const initialState = false;

export function getUser(state = initialState, action) {
  switch (action.type) {
    case userConstants.GET_USER_REQUEST:
      return {
        user: action
      };
    case userConstants.GET_USER_SUCCESS:
      return {
        user: action
      };
    case userConstants.GET_USER_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}