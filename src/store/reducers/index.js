import { combineReducers } from 'redux';

import { getUser } from './user.reducer';

const rootReducer = combineReducers({
  getUser,
});

export default rootReducer;