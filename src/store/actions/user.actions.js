import { userConstants } from '../constants/user.constants';
import { USER_API } from '../../config/config';

export const userActions = {
    getUserRepos
};

function getUserRepos(user) {

    return dispatch => {
        dispatch(request({ user }));

        fetch(USER_API + user + "/repos")
        .then( response => {
            if (!response.ok) { 
                dispatch(failure({"message": "Utilisateur non trouvé"})); throw response 
            } 
            return response.json() 
          })
            .then((userData) => {
                if(userData.length > 0)
                    dispatch(success(userData)); 
                else
                    dispatch(failure({"message": " Liste repo vide"}));
            } 
        )};
}

function request(user) { return { type: userConstants.GET_USER_REQUEST, user } }
function success(data) { return { type: userConstants.GET_USER_SUCCESS, data } }
function failure(error) { return { type: userConstants.GET_USER_FAILURE, error } }
