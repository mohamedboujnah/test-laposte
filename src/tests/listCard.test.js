import { render, screen, fireEvent } from '@testing-library/react';
import Search from '../components/elements/search/search';
import { Provider } from 'react-redux';
import { store } from '../store/store';
import ListCard from '../components/containers/listCard/listCard';

import * as reactRedux from 'react-redux'


describe('ListCard', () => {

  const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
  const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
  beforeEach(() => {
    useSelectorMock.mockClear() 
    useDispatchMock.mockClear()
  })

  test('Should return Chargement...', async () => {
    useSelectorMock.mockReturnValue({
      user: []
    })
    render(<Provider store={store}><ListCard /></Provider>);
    expect(await screen.findByText(/Chargement.../)).toBeInTheDocument();
  });

  test('Should return error message', async () => {
    useSelectorMock.mockReturnValue({
      error: {"message": "no user found"}
    })
    render(<Provider store={store}><ListCard /></Provider>);
    //screen.debug();
    expect(await screen.findByText(/no user found/)).toBeInTheDocument();
  });

  test('Should return data', async () => {

    useSelectorMock.mockReturnValue({
      user: {data: [
        {
          "id": 27264244,
          "name": "test-src",
          "full_name": "SonarTech/test-src",
          "watchers": 0,
          "language": "Java",
        }
        ]}
    })

    render(<Provider store={store}><ListCard /></Provider>);
    
    expect(await screen.findByText(/Java/)).toBeInTheDocument();

    //screen.debug();

  });
  
});
