import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Search from '../components/elements/search/search';
import { Provider } from 'react-redux';
import { store } from '../store/store';


describe('Search', () => {

  const props = {
    value: "",
    onChange: jest.fn(),
    onSubmit: jest.fn(e => e.preventDefault),
    error: "there is a error"
  }

  test('Should onchange to Have Been Called', async () => {

    render(<Provider store={store}><Search {...props} /></Provider>);
 
    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: 'JavaScript' },
    });
 
    expect(props.onChange).toHaveBeenCalledTimes(1);

    await userEvent.type(screen.getByRole('textbox'), 'test');

    expect(props.onChange).toHaveBeenCalled();
    expect(props.onChange).toHaveBeenCalledTimes(5);

  });


  test('Should click button Have Been Called', async () => {

    render(<Provider store={store}><Search {...props} /></Provider>);
    
    // click button
    fireEvent.submit(screen.getByRole('button'));
    expect(props.onSubmit).toHaveBeenCalled();

  });

  test('Should display error', async () => {

    render(<Provider store={store}><Search {...props} /></Provider>);
    expect(await screen.findByText(/there is a error/)).toBeInTheDocument();

  });

  
});
